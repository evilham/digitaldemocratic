# Project Management guidelines

In order to work in a mostly harmonic fashion we need to agree on some
general guidelines in how to manage things like issues, labels, etc.

## Labels

Here we document the labels available in this project and how they ought to
be used to organise and prioritise work.

Please propose any changes through a Merge Request changing this file that can
be reviewed and commented.

> Any label not listed on this document will be removed in the second half
> of June 2022.

### Priority

How an issue should prioritised regarding its importance; note that this
together with `stage::` marks what should be worked on first.

- `priority::0-wishlist`
- `priority::1-low`
- `priority::2-medium`
- `priority::3-high`
- `priority::4-critical`

### Stages

Used to have a high-level overview around particular dates or deadlines; e.g.
congress, school start, summer pause, ...

- `stage::2022_06`
- `stage::2022_07_congress`
- `stage::2022_08_summer`
- `stage::2022_09_school_start`
- `stage::far_future`

### Component

Which parts of the whole system, if any, are affected by this issue.

> Note that if the issue is in the systems part of a component, we will
> also apply the label `kind::sysadmin`.

- `component::Menu_Top&Bottom_Bar`
- `component::administration_app`
- `component::api`
- `component::bigbluebutton`
- `component::keycloak`
- `component::monitorization`
- `component::moodle`
- `component::nextcloud`
- `component::nextcloud-forms`
- `component::notifications`
- `component::onlyoffice`
- `component::security-proxy`
- `component::sistemas`
- `component::wordpress`

### Kind

General classification of the kind of issue.

- `kind::automation`
- `kind::bug`
- `kind::documentation`
- `kind::feature`
- `kind::security`
- `kind::sysadmin`
- `kind::testing`

### Workflow

Solving issues should follow this cycle:

- `workflow::0-triage`
- `workflow::1-todo`
- `workflow::2-in_progress`
- `workflow::3-done_to_review`
- `workflow::4-in_testing`
- `workflow::5-ready_to_deploy`
