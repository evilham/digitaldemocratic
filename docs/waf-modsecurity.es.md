# DD - Apache2 ModSecurity + HAProxy

Instalación de los servicios Apache2 ModSecurity y HAProxy.

* En el servicio de Apache2 con ModSecurity V3 se incluyen las reglas OWASP
* En servicio HAProxy actua de frontend de la aplicación, gestiona y negocia el certificado del dominio a través de letsencrypt.
* En la instalación el ModSecurity se encuentra deshabilitado para no interferir en el proceso de setup inicial del DD.
* La instalación se puede realizar con o sin la parte WAF.
* Si tenemos instalado el WAF podemos tenerlo activo o en modo bypass

## Apache - ModSecurity

Podemos encontrar la definición del servicio en `dd-sso/docker/waf-modsecurity`.

Tenemos diferentes ficheros para configurar este servicio:

* En el fichero `000-default.conf` tendremos la configuración del servidor web Apache2.
* En el fichero `crs-setup.conf` configuramos OWASP ModSecurity Core Rule Set ver.3.2.0
* En el fichero `modsec_rules.conf` incluimos los ficheros necesarios del owasp servicio de Apache2
* En el fichero `rules_apps.conf` se configuran los falsos positivos, de las diferentes aplicaciones, que se tienen idenficados hasta el momento.

### Enable/Disable

DD se puede utilizar con WAF tenerlo activo o desactivado, esto se gestiona
con la variable `DISABLE_WAF` en `dd.conf`.

El valor por defecto actual es `true` (WAF desactivado),
esto cambiará en un futuro.

```
# Ejemplo de dd.conf

# Usar WAF
DISABLE_WAF=false

# No usar WAF
DISABLE_WAF=false
```

### Configuración

Los cambios en `dd.conf` no son inmediatos,
tenemos que re-desplegar los contenedores de DD usando `dd-ctl`:

```sh
./dd-ctl down
./dd-ctl build
./dd-ctl up
```
