# Instal·lació

## Requisits

Distribució de Linux, principalment basada en [Debian](https://debian.org)
capaç d'executar docker-compose v1.28 o més nova.

## Configuració

És convenient familiaritzar-se amb les configuracions disponibles a
[`dd.conf.sample`][dd.conf.sample], que és el principal lloc de configuració.

[dd.conf.sample]: https://gitlab.com/DD-workspace/DD/-/blob/main/dd.conf.sample

La instal·lació modifica el fitxer dd.conf per dd.conf.sample (el substitueix). Idealment inicialitza les variables des de la comanda dd-install.sh:

```
> DD_NETWORK_MTU=1450 ./dd-install.sh
```
## Interactivament

Podem fer servir l'script [`dd-install.sh`][dd-install.sh] sense arguments per
procedir amb la instal·lació interactiva.

[dd-install.sh]: https://dd-work.space/docs/dd-install.sh

L'instal·lador de la línia de comandes farà totes les preguntes necessàries
per realitzar la instal·lació.

Un cop finalitzada la instal·lació podem procedir amb la
[post-instal·lació](post-install.ca.md).

### Exemple interactiu

```bash
# We obtain dd-install.sh
> wget https://dd-work.space/docs/dd-install.sh -O dd-install.sh
# Make it executable
> chmod +x dd-install.sh
# And run it
> ./dd-install.sh
Interactive install detected!
Please follow the instructions carefully:

Under which DOMAIN will you install DD? example.org


You will need to setup DNS entries for:
- [ ] moodle.dd.004.es
- [ ] nextcloud.dd.004.es
- [ ] wp.dd.004.es
- [ ] oof.dd.004.es
- [ ] sso.dd.004.es
- [ ] pad.dd.004.es
- [ ] admin.dd.004.es
- [ ] api.dd.004.es
- [ ] correu.dd.004.es


What is the short title of the DD instance? [DD]
What is the full title of the DD instance? [DD] DD at example.org
Do you want to use Let's Encrypt certificates? [Y/n] Y
Which email will you use for Let's Encrypt notifications? letsencrypt@example.org
Generate a certificate for example.org? (neds the DNS entry) [y/N] N
Path to the logo's PNG file (optional):
Path to the background's PNG file (optional):
About to install with following information:

DOMAIN=example.org
TITLE_SHORT=DD
TITLE=DD at example.org
LETSENCRYPT_DNS=example.org
LETSENCRYPT_EMAIL=letsencrypt@example.org
LETSENCRYPT_DOMAIN_ROOT=false

Custom logo in PNG (if requested):
Custom background in PNG (if requested):
Is this correct? proceed with the install? [Y/n] Y
```
<details><summary>Logos</summary>
Opcionalment es poden indicar els logos ubicant els fitxers <code>.png</code>
al servidor i indicant la seva ruta quan l'instal·lador les demana.
</details>

<details><summary>Certificat preexistent</summary>
Tens la posibilitat d'utilitzar el teu propi certificat, ja sigui wildcard o bé SAN. Pots llegir-ne mes a l'apartat [wildcard](wildcard.ca.md).
</details>

## Automatitzat

L'instal·lador accepta totes les variables del fitxer
[`dd.conf.sample`][dd.conf.sample] com a variables d'entorn del mateix nom
però amb el prefix `DD_` per distingir-les d'altres variables d'entorn.

Per exemple, la variable `DOMAIN` es pot pre-configurar per l'instal·lador
amb la variable d'entorn `DD_DOMAIN`.
Vegeu l'exemple automàtic tot seguit.

Si `DD_DOMAIN` està definit la instal·lació es fa de forma automatitzada amb la
informació de les variables `DD_*` corresponents a les configuracions de
[`dd.conf.sample`][dd.conf.sample].
D'altra manera, amb `DD_DOMAIN` no definit, es procedirà de manera interactiva. 

Tingueu en compte que per la instal·lació automatitzada, és necessari haver
creat les entrades de DNS que apuntin al vostre servidor.
A l'exemple interactiu més amunt, podeu consultar el llistat sencer d'entrades
DNS.

> Val la pena remarcar que aquesta forma d'instal·lació és succeptible de ser
> automatitzada amb eines d'aprovisionament, com ara [Ansible][ansible],
> [cdist][cdist] o [Puppet][puppet].

[ansible]: https://ansible.com
[cdist]: https://cdi.st
[puppet]: https://puppet.com

Un cop finalitzada la instal·lació podem procedir amb la
[post-instal·lació](post-install.ca.md).

### Exemple automàtic

```bash
> export DD_DOMAIN="example.org"
> export DD_TITLE="DD at example.org"
> export DD_TITLE_SHORT="DD"
> export DD_LETSENCRYPT_DNS="example.org"
> export DD_LETSENCRYPT_EMAIL="letsencrypt@example.org"
#
# 
# We obtain dd-install.sh
> wget https://dd-work.space/docs/dd-install.sh -O dd-install.sh
# Make it executable
> chmod +x dd-install.sh
# And run it
> ./dd-install.sh
```

<details><summary>Logos</summary>
Opcionalment es poden indicar els logos ubicant els fitxers <code>.png</code>
al servidor i apuntant les variables d'entorn <code>DDIMG_LOGO_PNG</code> i
<code>DDIMG_BACKGROUND_PNG</code> a la ruta on es troben aquests fitxers.
</details>

## Manualment

La instal·lació manual està documentada en el mateix [`dd-install.sh`][repo-dd-install.sh] a partir de la línia:

[repo-dd-install.sh]: https://gitlab.com/DD-workspace/DD/-/blob/main/dd-install.sh

```bash
#
# START MANUAL INSTALL
#
```

Hi trobareu comentaris de cada pas juntament amb les comandes respectives.

Un cop finalitzada la instal·lació podem procedir amb la [post-instal·lació](post-install.ca.md).
