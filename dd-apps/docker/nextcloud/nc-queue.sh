#!/bin/sh

find "${NC_MAIL_QUEUE_FOLDER:-/nc-mail-queue}" -name '*.sh' -exec sh -c \
	'i="$1"; "$i" && rm "$i"' shell {} \
	';'
