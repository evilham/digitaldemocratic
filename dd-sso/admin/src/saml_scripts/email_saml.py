#
#   Copyright © 2022 Evilham
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import os
from typing import Any, Dict

from saml_service import SamlService


class EmailSaml(SamlService):
    client_description: str = "Client for the DD-managed email service"
    email_domain: str

    def __init__(self, email_domain: str):
        super(EmailSaml, self).__init__(client_name="correu")
        self.email_domain = email_domain

    @property
    def enabled(self) -> bool:
        return bool(self.email_domain)

    def configure(self) -> None:
        srv_base = f"https://correu.{self.domain}"
        client_id = f"{srv_base}/metadata/"
        client_overrides: Dict[str, Any] = {
            "name": self.client_name,
            "description": self.client_description,
            "clientId": client_id,
            "baseUrl": f"{srv_base}/login",
            "enabled": self.enabled,
            "redirectUris": [
                f"{srv_base}/*",
            ],
            "webOrigins": [srv_base],
            "consentRequired": False,
            "protocol": "saml",
            "attributes": {
                "saml.assertion.signature": True,
                "saml_idp_initiated_sso_relay_state": f"{srv_base}/login",
                "saml_assertion_consumer_url_redirect": f"{srv_base}/acs",
                "saml.force.post.binding": True,
                "saml.multivalued.roles": False,
                "saml.encrypt": False,
                "saml_assertion_consumer_url_post": f"{srv_base}/acs",
                "saml.server.signature": True,
                "saml_idp_initiated_sso_url_name": f"{srv_base}/acs",
                "saml.server.signature.keyinfo.ext": False,
                "exclude.session.state.from.auth.response": False,
                "saml_single_logout_service_url_redirect": f"{srv_base}/ls",
                "saml.signature.algorithm": "RSA_SHA256",
                "saml_force_name_id_format": False,
                "saml.client.signature": False,
                "tls.client.certificate.bound.access.tokens": False,
                "saml.authnstatement": True,
                "display.on.consent.screen": False,
                "saml_name_id_format": "username",
                "saml_signature_canonicalization_method": "http://www.w3.org/2001/10/xml-exc-c14n#",
                "saml.onetimeuse.condition": False,
            },
            "protocolMappers": [
                {
                    "name": "username",
                    "protocol": "saml",
                    "protocolMapper": "saml-user-property-mapper",
                    "consentRequired": False,
                    "config": {
                        "attribute.nameformat": "Basic",
                        "user.attribute": "username",
                        "friendly.name": "username",
                        "attribute.name": "username",
                    },
                },
                {
                    "name": "email",
                    "protocol": "saml",
                    "protocolMapper": "saml-user-property-mapper",
                    "consentRequired": False,
                    "config": {
                        "attribute.nameformat": "Basic",
                        "user.attribute": "email",
                        "friendly.name": "email",
                        "attribute.name": "email",
                    },
                },
            ],
        }
        self.set_client(client_id, client_overrides)


if __name__ == "__main__":
    import logging as log
    email_domain = os.environ.get("MANAGED_EMAIL_DOMAIN", "")
    log.info("Configuring SAML client for Email")
    EmailSaml(email_domain=email_domain).configure()
