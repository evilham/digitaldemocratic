#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
ARG HAPROXY_IMG
FROM $HAPROXY_IMG as production

USER root
RUN apk add openssl certbot py-pip
RUN pip install certbot-plugin-gandi

COPY letsencrypt-hook-deploy-concatenante.sh /usr/local/sbin/
COPY letsencrypt.sh /usr/local/sbin/
COPY letsencrypt-renew-cron.sh /etc/periodic/daily/letsencrypt-renew
COPY auto-generate-certs.sh /usr/local/sbin/

COPY docker-entrypoint.sh /usr/local/bin/
RUN ln -s /usr/local/bin/docker-entrypoint.sh /
RUN chmod 555 docker-entrypoint.sh

COPY haproxy.cnf.parts /haproxy.cnf.parts
ADD gen-haproxy-conf.sh /gen-haproxy-conf.sh
RUN chmod 555 gen-haproxy-conf.sh
# Generate all flavours of configuration
RUN /gen-haproxy-conf.sh    waf no-proxy > /usr/local/etc/haproxy/haproxy.waf.no-proxy.cfg
RUN /gen-haproxy-conf.sh    waf    proxy > /usr/local/etc/haproxy/haproxy.waf.proxy.cfg
RUN /gen-haproxy-conf.sh no-waf no-proxy > /usr/local/etc/haproxy/haproxy.no-waf.no-proxy.cfg
RUN /gen-haproxy-conf.sh no-waf    proxy > /usr/local/etc/haproxy/haproxy.no-waf.proxy.cfg
