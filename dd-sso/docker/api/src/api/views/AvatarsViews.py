#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import json
import logging as log
import os
import sys
import time
import traceback
from typing import TYPE_CHECKING
from uuid import uuid4

from flask import (
    Response,
    jsonify,
    redirect,
    render_template,
    request,
    send_from_directory,
    url_for,
)

if TYPE_CHECKING:
    from api.flaskapp import ApiFlaskApp

from ..lib.avatars import Avatars

avatars = Avatars()


def setup_avatar_views(app: "ApiFlaskApp") -> None:
    # TODO: check if this is redundant and remove it
    @app.route("/avatar/<username>", methods=["GET"])
    def avatar(username: str) -> Response:
        return send_from_directory(
            os.path.join(app.root_path, "../avatars/master-avatars/"),
            avatars.get_user_avatar(username),
            mimetype="image/jpg",
        )
